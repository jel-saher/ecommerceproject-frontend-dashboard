import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddproductComponent } from './components/addproduct/addproduct.component';
import { AddsubcategoryComponent } from './components/addsubcategory/addsubcategory.component';
import { DetailproductComponent } from './components/detailproduct/detailproduct.component';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './components/layout/layout.component';
import { ListproductComponent } from './components/listproduct/listproduct.component';
import { ListsubcategoryComponent } from './components/listsubcategory/listsubcategory.component';
import { LoginComponent } from './components/login/login.component';
import { ProfilComponent } from './components/profil/profil.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: 'home',
    canActivate: [AuthGuard],
    component: HomeComponent,
    children: [
      { path: '', component: LayoutComponent },
      { path: 'addproduct', component: AddproductComponent },
      { path: 'listproduct', component: ListproductComponent },
      { path: 'detailproduct/:id', component: DetailproductComponent },
      { path: 'addproduct', component: AddproductComponent },
      { path: 'listsubcategory', component: ListsubcategoryComponent },
      { path: 'addsubcategory', component: AddsubcategoryComponent },
      { path: 'profil', component: ProfilComponent },
    ],
  },
  { path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
