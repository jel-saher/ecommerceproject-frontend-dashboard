import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });

  getallproduct() {
    return this.http.get(`${environment.baseurl}/product/getallproduct`);
  }

  deleteProduct(id: any) {
    return this.http.delete(
      `${environment.baseurl}/product/deleteproduct/${id}`
    );
  }
  getOneProduct(id: any) {
    return this.http.get(`${environment.baseurl}/product/getbyidproduct/${id}`);
  }

  updateProduct(id: any, newProduct: any) {
    return this.http.put(
      `${environment.baseurl}/product/updateproduct/${id}`,
      newProduct
    );
  }

  addproduct(product: any) {
    return this.http.post(
      `${environment.baseurl}/product/createproduct`,
      product,
      { headers: this.headersoption }
    );
  }
}
