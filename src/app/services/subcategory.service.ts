import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SubcategoryService {
  constructor(private http: HttpClient) {}
  getallsubcategory() {
    return this.http.get(
      `${environment.baseurl}/subcategory/getallsubcategory`
    );
  }

  deletesubcategory(id: any) {
    return this.http.delete(
      `${environment.baseurl}/subcategory/deletesubcategory/${id}`
    );
  }

  updatesubcategory(id: any, newsubcategory: any) {
    return this.http.put(`${environment.baseurl}/subcategory/updatesubcategory/${id}`, newsubcategory);
  }

  getonesubcategory(id: any) {
    return this.http.get(
      `${environment.baseurl}/subcategory/getbyidsubcategory/${id}`
    );
  }

  createsubcategory(sub: any) {
    return this.http.post(
      `${environment.baseurl}/subcategory/createsubcategory`,
      sub
    );
  }
}
