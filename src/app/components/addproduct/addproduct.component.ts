import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from 'src/app/services/product.service';
import { ProviderService } from 'src/app/services/provider.service';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css'],
})
export class AddproductComponent implements OnInit {
  productForm: FormGroup;
  submitted = false;
  listprovider: any;
  listsubcategory: any;
  myFiles: string[] = [];
  //fileToUpload: Array<File> = [];

  constructor(
    private formBuilder: FormBuilder,
    private serviceproduct: ProductService,
    private providerservice: ProviderService,
    private subcategoryservice: SubcategoryService
  ) {}

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      name: ['', Validators.required],
      prix: ['', Validators.required],
      quantite: ['', Validators.required],
      ref: ['', Validators.required],
      description: ['', Validators.required],
      subcategory: ['', Validators.required],
      provider: ['', Validators.required],
      pictures: ['', Validators.required],
    });
    this.getallprovider();
    this.getallsubcategory();
  }

  /*  handleFileInput(files: any) {
    this.fileToUpload = <Array<File>>files.target.files;
    console.log(this.fileToUpload);
  } */

  get f() {
    return this.productForm.controls;
  }

  onFileChange(event: any) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
    }
  }

  onSubmit() {
    this.submitted = true;
    let token = localStorage.getItem('token')!;
    console.log('ezg : ', token);

    let formdata = new FormData();
    formdata.append('name', this.productForm.value.name);
    formdata.append('prix', this.productForm.value.prix);
    formdata.append('quantite', this.productForm.value.quantite);
    formdata.append('ref', this.productForm.value.ref);
    formdata.append('description', this.productForm.value.description);
    formdata.append('subcategory', this.productForm.value.subcategory);
    formdata.append('pictures', this.productForm.value.provider);

    for (var i = 0; i < this.myFiles.length; i++) {
      formdata.append('pictures', this.myFiles[i]);
      console.log('ressss');
    }

    // stop here if form is invalid

    this.serviceproduct.addproduct(formdata).subscribe(
      (res: any) => {
        console.log('reponse ', res);
        if ((res.status = 200)) {
          Swal.fire({ icon: 'success', title: 'Your Product is created' });
        }
      },
      (err) => {
        Swal.fire({ icon: 'error', title: 'Your Product is not created' });
      }
    );
  }

  onReset() {
    this.submitted = false;
    this.productForm.reset();
  }

  getallprovider() {
    this.providerservice.getallprovider().subscribe((res: any) => {
      this.listprovider = res['data'];
      console.log('liste provider : ', this.listprovider);
    });
  }

  getallsubcategory() {
    this.subcategoryservice.getallsubcategory().subscribe((res: any) => {
      this.listsubcategory = res['data'];
      console.log('liste subcategory : ', this.listsubcategory);
    });
  }
}
