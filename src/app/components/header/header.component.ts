import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  constructor() {}

  ngOnInit(): void {}

  logout() {
    localStorage.clear();
  }
}
