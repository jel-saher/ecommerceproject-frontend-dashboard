import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private loginservice: LoginService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login() {
    this.submitted = true;
    this.loginservice.login(this.loginForm.value).subscribe(
      (res: any) => {
        console.log('Login ', res);
        if ((res.status = 200)) {
          Swal.fire({
            icon: 'success',
            title: 'user found',
            text: 'email valid',
            footer: 'password valid',
          });
          this.route.navigateByUrl('/home');
          localStorage.setItem('userconnect', JSON.stringify(res.user));
          localStorage.setItem('token', res.token);
          localStorage.setItem('state', '0');
        }
      },
      (err) => {
        Swal.fire({
          icon: 'error',
          title: 'user not found',
          text: 'email invalid',
          footer: 'password invalid',
        });
      }
    );
  }

  onReset() {
    this.submitted = false;
    this.loginForm.reset();
  }
}
