import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listproduct',
  templateUrl: './listproduct.component.html',
  styleUrls: ['./listproduct.component.css'],
})
export class ListproductComponent implements OnInit {
  searchname :any;
  listproduct: any;
  p: number = 1;
  productForm: FormGroup;
  closeResult = '';

  constructor(
    private productservice: ProductService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      _id: ['', Validators.required],
      name: ['', Validators.required],
      prix: ['', Validators.required],
      quantite: ['', Validators.required],
      ref: ['', Validators.required],
      description: ['', Validators.required],
      color:  ['', Validators.required],
    });

    this.getallproduct();
  }

  getallproduct() {
    this.productservice.getallproduct().subscribe((res: any) => {
      this.listproduct = res['data'];
    });
  }

  deleteProduct(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.productservice.deleteProduct(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.getallproduct();
        });
      }
    });
  }

  open(content: any, product: any) {
    this.productForm.patchValue({
      _id: product._id,
      name: product.name,
      prix: product.prix,
      quantite: product.quantite,
      ref: product.ref,
      description: product.description,
      color: product.color,
    });

    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  updateProduct() {
    this.productservice
      .updateProduct(this.productForm.value._id, this.productForm.value)
      .subscribe((res: any) => {
        console.log(res);
        Swal.fire('Product Updated');
        this.getallproduct();
      });
  }
}
