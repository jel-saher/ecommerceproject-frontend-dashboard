import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from 'src/app/services/category.service';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listsubcategory',
  templateUrl: './listsubcategory.component.html',
  styleUrls: ['./listsubcategory.component.css'],
})
export class ListsubcategoryComponent implements OnInit {
  listsub: any;
  searchname: any;
  p = 0;
  subForm: FormGroup;
  closeResult = '';
  listcategory: any;

  constructor(
    private subcategoryservice: SubcategoryService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private categoryservice: CategoryService
  ) {}

  ngOnInit(): void {
    this.subForm = this.formBuilder.group({
      _id: ['', Validators.required],
      name: ['', Validators.required],
      description: ['', Validators.required],
      category: ['', Validators.required],
    });
    this.getallsubcategory();
    this.getallcategory();
  }

  getallsubcategory() {
    this.subcategoryservice.getallsubcategory().subscribe((res: any) => {
      this.listsub = res['data'];
    });
  }

  // delete //

  deletesubcategory(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.subcategoryservice.deletesubcategory(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          this.getallsubcategory();
        });
      }
    });
  }

// update //

  open(content: any, sub: any) {
    this.subForm.patchValue({
      _id: sub._id,
      name: sub.name,
      description: sub.description,
      category: sub.category.name
    });

    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  updatesubcategory() {
    this.subcategoryservice
      .updatesubcategory(this.subForm.value._id, this.subForm.value)
      .subscribe((res: any) => {
        console.log(res);
        Swal.fire('Subcategory Updated');
        this.getallsubcategory();
      });
  }


  
  getallcategory() {
    this.categoryservice.getallcategory().subscribe((res: any) => {
      this.listcategory = res['data'];
    });
  }
}
