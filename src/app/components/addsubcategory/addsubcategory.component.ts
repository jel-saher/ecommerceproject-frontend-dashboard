import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from 'src/app/services/category.service';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addsubcategory',
  templateUrl: './addsubcategory.component.html',
  styleUrls: ['./addsubcategory.component.css'],
})
export class AddsubcategoryComponent implements OnInit {
  subForm: FormGroup;
  submitted = false;
  listcategory: any;

  constructor(
    private formBuilder: FormBuilder,
    private subservice: SubcategoryService,
    private categoryservice: CategoryService
  ) {}

  ngOnInit(): void {
    this.subForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: [''],
      category: ['', Validators.required],
    });
    this.getallcategory();
  }

  get f() {
    return this.subForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.subForm.invalid) {
      return;
    }

    this.subservice
      .createsubcategory(this.subForm.value)
      .subscribe((res: any) => {});

    Swal.fire('Created', 'Your Subcategory has been created.', 'success');
    this.onReset();

    // display form values on success
    /* alert(
      'SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4)
    ); */
  }

  onReset() {
    this.submitted = false;
    this.subForm.reset();
  }

  getallcategory() {
    this.categoryservice.getallcategory().subscribe((res: any) => {
      this.listcategory = res['data'];
    });
  }
}
