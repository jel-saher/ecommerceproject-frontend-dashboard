import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css'],
})
export class ProfilComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  constructor() {}

  ngOnInit(): void {
    console.log(this.userconnect);
  }
}
