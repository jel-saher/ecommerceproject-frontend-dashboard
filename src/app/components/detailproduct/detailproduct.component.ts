import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-detailproduct',
  templateUrl: './detailproduct.component.html',
  styleUrls: ['./detailproduct.component.css'],
})
export class DetailproductComponent implements OnInit {
  list: any;
  id = this.activateroute.snapshot.params['id'];

  constructor(
    private productservice: ProductService,
    private activateroute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getoneproduct();
  }
  getoneproduct() {
    this.productservice.getOneProduct(this.id).subscribe((res: any) => {
      this.list = res['data'];
      console.log('Detail :  ', this.list);
    });
  }
}
