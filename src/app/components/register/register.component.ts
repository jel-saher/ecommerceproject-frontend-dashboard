import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from 'helper/mustmatch';
import { RegisterService } from 'src/app/services/register.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  fileToUpload: Array<File> = [];

  constructor(
    private formBuilder: FormBuilder,
    private registerservice: RegisterService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group(
      {
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        adresse: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required],
      },
      {
        validator: MustMatch('password', 'confirmPassword'),
      }
    );
  }

  handleFileInput(files: any) {
    this.fileToUpload = <Array<File>>files.target.files;
    console.log(this.fileToUpload);
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    
    this.submitted = true;

    let formdata = new FormData();
    formdata.append('firstname', this.registerForm.value.firstname);
    formdata.append('lastname', this.registerForm.value.lastname);
    formdata.append('adresse', this.registerForm.value.adresse);
    formdata.append('email', this.registerForm.value.email);
    formdata.append('password', this.registerForm.value.password);
    formdata.append('image', this.fileToUpload[0]);

    

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.registerservice.register(formdata).subscribe((res: any) => {});

    Swal.fire('Created', 'Your account has been created.', 'success');
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
