import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LayoutComponent } from './components/layout/layout.component';
import { HomeComponent } from './components/home/home.component';
import { AddproductComponent } from './components/addproduct/addproduct.component';
import { ListproductComponent } from './components/listproduct/listproduct.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailproductComponent } from './components/detailproduct/detailproduct.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RecherchePipe } from './pipes/recherche.pipe';
import { AddsubcategoryComponent } from './components/addsubcategory/addsubcategory.component';
import { ListsubcategoryComponent } from './components/listsubcategory/listsubcategory.component';
import { DetailsubcategoryComponent } from './components/detailsubcategory/detailsubcategory.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfilComponent } from './components/profil/profil.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    LayoutComponent,
    HomeComponent,
    AddproductComponent,
    ListproductComponent,
    DetailproductComponent,
    RecherchePipe,
    AddsubcategoryComponent,
    ListsubcategoryComponent,
    DetailsubcategoryComponent,
    LoginComponent,
    RegisterComponent,
    ProfilComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    
    
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
